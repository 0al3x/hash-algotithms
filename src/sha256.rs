use crate::hash::Hash;
use std::convert::TryInto;

const PADDING_FACTOR: u64 = 64;
const BITS_PER_BYTE: u64 = 8;


struct Sha256 {
    input: Vec<[u8; 64]>,
    consts: [u32; 64],
    hash: [u32; 8],
}

impl Hash for Sha256 {
    type Output = [u8; 32];
    type Error = ();

    fn new() -> Self {
        const K: [u32; 64] = [
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4,
            0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe,
            0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f,
            0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
            0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
            0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
            0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116,
            0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
            0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7,
            0xc67178f2,
        ];

        Self {
            input: Vec::with_capacity(0),
            consts: K,
            hash: [
                0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab,
                0x5be0cd19,
            ],
        }
    }

    fn input(&mut self, input: &[u8]) {
        let len = input.len();

        let first_byte: u8 = 0x80;
        let last_bytes = ((len % PADDING_FACTOR as usize) as u64 * BITS_PER_BYTE).to_be_bytes();

        // FIXME: still to be fixed :'(
        //let padding: usize = (56isize - (len as isize * 3isize)).abs() as usize;
        let padding = ((56isize - len as isize) % 56isize).abs() as usize;

        let mut input = input.to_vec();

        input.push(first_byte);
        input.extend(&vec![0u8; padding - 1]);
        input.extend(last_bytes);

        assert!(input.len() % 64 == 0);

        let n = input.len() / 64;

        self.input = Vec::<[u8; 64]>::with_capacity(n);

        for i in 0..n {
            let start = i * 64;
            let end = (i + 1) * 64;

            self.input.push(input[start..end].try_into().unwrap());
        }
    }

    fn input_str(&mut self, input: &str) {
        let input = input.as_bytes();
        self.input(input);
    }

    fn finalize(&mut self) -> Result<Self::Output, Self::Error> {
        for b in &self.input {
            let mut w = [0u32; 64];

            for i in 0..16 {
                let start = i * 4usize;

                w[i] = u32::from_be_bytes(b[start..start + 4].try_into().unwrap());
            }

            for i in 16..64 {
                let s0 =
                    (w[i - 15].rotate_right(7)) ^ (w[i - 15].rotate_right(18)) ^ (w[i - 15] >> 3);

                let s1 =
                    (w[i - 2].rotate_right(17)) ^ (w[i - 2].rotate_right(19)) ^ (w[i - 2] >> 10);

                w[i] = w[i - 16]
                    .overflowing_add(s0)
                    .0
                    .overflowing_add(w[i - 7])
                    .0
                    .overflowing_add(s1)
                    .0;
            }

            let mut a = self.hash[0];
            let mut b = self.hash[1];
            let mut c = self.hash[2];
            let mut d = self.hash[3];
            let mut e = self.hash[4];
            let mut f = self.hash[5];
            let mut g = self.hash[6];
            let mut h = self.hash[7];

            for i in 0..64 {
                let s1 = (e.rotate_right(6)) ^ (e.rotate_right(11)) ^ (e.rotate_right(25));

                let ch = (e & f) ^ (!e & g);

                let temp1 = h
                    .overflowing_add(s1)
                    .0
                    .overflowing_add(ch)
                    .0
                    .overflowing_add(self.consts[i])
                    .0
                    .overflowing_add(w[i])
                    .0;

                let s0 = (a.rotate_right(2)) ^ (a.rotate_right(13)) ^ (a.rotate_right(22));

                let maj = (a & b) ^ (a & c) ^ (b & c);

                let temp2 = s0.overflowing_add(maj).0;

                h = g;
                g = f;
                f = e;
                e = d.overflowing_add(temp1).0;
                d = c;
                c = b;
                b = a;
                a = temp1.overflowing_add(temp2).0;
            }

            self.hash[0] = self.hash[0].overflowing_add(a).0;
            self.hash[1] = self.hash[1].overflowing_add(b).0;
            self.hash[2] = self.hash[2].overflowing_add(c).0;
            self.hash[3] = self.hash[3].overflowing_add(d).0;
            self.hash[4] = self.hash[4].overflowing_add(e).0;
            self.hash[5] = self.hash[5].overflowing_add(f).0;
            self.hash[6] = self.hash[6].overflowing_add(g).0;
            self.hash[7] = self.hash[7].overflowing_add(h).0;
        }

        Ok([
            self.hash[0].to_be_bytes(),
            self.hash[1].to_be_bytes(),
            self.hash[2].to_be_bytes(),
            self.hash[3].to_be_bytes(),
            self.hash[4].to_be_bytes(),
            self.hash[5].to_be_bytes(),
            self.hash[6].to_be_bytes(),
            self.hash[7].to_be_bytes(),
        ]
        .concat()
        .try_into()
        .unwrap())
    }

    fn finalize_str(&mut self) -> Result<String, Self::Error> {
        let hash = self.finalize()?;

        Ok(hash.iter().map(|b| format!("{b:02x}")).collect())
    }
}

#[test]
fn tests() {
    let input = b"Hola Mundo";
    let mut hasher = Sha256::new();
    hasher.input(input);
    let output = hasher.finalize_str().expect("");
    let expected = "c3a4a2e49d91f2177113a9adfcb9ef9af9679dc4557a0a3a4602e1bd39a6f481";
    assert_eq!(expected, &output);

    let input = "¿Por qué decidí hacer esto?".as_bytes();
    let mut hasher = Sha256::new();
    hasher.input(input);
    let output = hasher.finalize_str().expect("");
    let expected = "2b9bf86e4a3fa539081ee950ca396dca600439f37996c1687d034de3776b0edf";
    assert_eq!(expected, output);

    let input = b"testcase";
    let mut hasher = Sha256::new();
    hasher.input(input);
    let output = hasher.finalize_str().expect("");
    let expected = "5659f2efca07c82f1901d48812fdbec3ee923966c789e7f6e610ffc4c4933d91";
    assert_eq!(expected, output);

    let input = b"Ya me rindo";
    let mut hasher = Sha256::new();
    hasher.input(input);
    let output = hasher.finalize_str().expect("");
    let expected = "86a4e93966c56a8e6b9c373c7d84c3a9a963a22967d619e19b9c517584b9da4e";
    assert_eq!(expected, output);

    let input = "No puedo más".as_bytes();
    let mut hasher = Sha256::new();
    hasher.input(input);
    let output = hasher.finalize_str().expect("");
    let expected = "aabbba0c8be7d21eeaffb09079b472eb914274df1e211ef34501db3b0cf20c12";
    assert_eq!(expected, output);
}
