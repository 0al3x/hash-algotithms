
pub trait Hash {
    type Output;
    type Error;

    fn new() -> Self;

    fn input(&mut self, _: &[u8]);

    fn input_str(&mut self, _: &str);

    fn finalize(&mut self) -> Result<Self::Output, Self::Error>;

    fn finalize_str(&mut self) -> Result<String, Self::Error>;
}
